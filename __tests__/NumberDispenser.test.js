const NumberDispenser = require('../src/helpers/NumberDispenser');

test('1.000 random', () => {
  const total = 1000;
  const dispenser = new NumberDispenser(total);
  let sum = 0;

  for (let i = 0; i < 20; i++) {
    const numbers = dispenser.getRandomNumbers();
    sum += numbers;
  }
  expect(sum).toBe(total);
});

test('1.000 exact', () => {
  const total = 1000;
  const dispenser = new NumberDispenser(total);

  dispenser.tryGetExact(100);
  expect(dispenser.numbersLeft).toBe(900);

  dispenser.tryGetExact(1);
  expect(dispenser.numbersLeft).toBe(899);

  const numbers = dispenser.tryGetExact(900);
  expect(numbers).toBe(899);
  expect(dispenser.numbersLeft).toBe(0);
});

test('1.000 and get the rest', () => {
  const total = 1000;
  const dispenser = new NumberDispenser(total);

  dispenser.tryGetExact(100);
  expect(dispenser.numbersLeft).toBe(900);

  const number = dispenser.getTheRest();
  expect(number).toBe(900);
  expect(dispenser.numbersLeft).toBe(0);
});
