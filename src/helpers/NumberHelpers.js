class NumberHelpers {
  /**
   * Obtém número aleatório conforme
   * limite
   * @param {number} min Valor mínimo
   * @param {number} max Valor máximo
   */
  static getRandomNumberBetween(min, max) {
    const random = Math.random();
    const floor = Math.min(min + Math.round(random * max), max);
    const answer = floor;
    return answer;
  }
}

module.exports = NumberHelpers;
