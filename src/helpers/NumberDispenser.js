const NumberHelpers = require('./NumberHelpers');

class NumberDispenser {
  constructor(total) {
    this.total = total;
    this.left = total;
  }

  _subtract(numbers) {
    this.left -= numbers;
  }

  getRandomNumbers() {
    const numbers = NumberHelpers.getRandomNumberBetween(1, this.left);
    this._subtract(numbers);
    return numbers;
  }

  tryGetExact(exact) {
    const correctValue = Math.min(this.left, exact);
    this._subtract(correctValue);
    return correctValue;
  }

  getTheRest() {
    const theRest = this.left;
    this._subtract(this.left);
    return theRest;
  }

  get numbersLeft() {
    return this.left;
  }
}

module.exports = NumberDispenser;
